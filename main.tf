terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.19.1"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "ResourceGroup" {
  name     = "resource_group"
  location = "West Europe"
}

resource "azurerm_network_security_group" "SecurityGroup" {
  name                = "security_group"
  location            = azurerm_resource_group.ResourceGroup.location
  resource_group_name = azurerm_resource_group.ResourceGroup.name

  security_rule {
    name                       = "rule80"
    priority                   = 150
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "rule3389"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "rule5985"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "rule5986"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5986"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_virtual_network" "VirtualNetwork" {
  name                = "virtual_network"
  location            = azurerm_resource_group.ResourceGroup.location
  resource_group_name = azurerm_resource_group.ResourceGroup.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["10.0.0.4", "10.0.0.5"]

}

resource "azurerm_subnet" "Subnet" {
  name                 = "subnet"
  resource_group_name  = azurerm_resource_group.ResourceGroup.name
  virtual_network_name = azurerm_virtual_network.VirtualNetwork.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_public_ip" "PublicIPForLB" {
  name                = "PublicIPForLB"
  location            = azurerm_resource_group.ResourceGroup.location
  resource_group_name = azurerm_resource_group.ResourceGroup.name
  allocation_method   = "Static"
  sku = "Standard"
}

resource "azurerm_lb" "LoadBalancer" {
  name                = "LoadBalancer"
  location            = azurerm_resource_group.ResourceGroup.location
  resource_group_name = azurerm_resource_group.ResourceGroup.name
  sku = "Standard"

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.PublicIPForLB.id
  }
}

resource "azurerm_lb_nat_rule" "Nat80" {
  resource_group_name            = azurerm_resource_group.ResourceGroup.name
  loadbalancer_id                = azurerm_lb.LoadBalancer.id
  name                           = "Port80Access"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "PublicIPAddress"
}

resource "azurerm_lb_probe" "Probe" {
  loadbalancer_id = azurerm_lb.LoadBalancer.id
  name            = "http-running-probe"
  port            = 80
}

resource "azurerm_lb_backend_address_pool" "BackendAddressPool" {
  loadbalancer_id = azurerm_lb.LoadBalancer.id
  name            = "BackEndAddressPool"
}

resource "azurerm_network_interface_backend_address_pool_association" "example" {
  network_interface_id = azurerm_network_interface.NetworkInterface.id
  ip_configuration_name = "internal"
  backend_address_pool_id = azurerm_lb_backend_address_pool.BackendAddressPool.id
}

resource "azurerm_network_interface" "NetworkInterface" {
  name                = "NetworkInterface"
  location            = azurerm_resource_group.ResourceGroup.location
  resource_group_name = azurerm_resource_group.ResourceGroup.name
  
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.Subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}


resource "azurerm_windows_virtual_machine_scale_set" "VM1" {
  name                = "vm1"
  resource_group_name = azurerm_resource_group.ResourceGroup.name
  location            = azurerm_resource_group.ResourceGroup.location
  sku                 = "Standard_B1s"
  admin_username      = "adminuser"
  admin_password      = "P@$$w0rd1234!"
  instances           = 1
  zones = ["1", "2"]

  winrm_listener {
    protocol = "Https"
    certificate_url = "https://vault-name45644294.vault.azure.net:443/secrets/somename/abb8ebf911a145ccacca758026efacca"
  }

  secret {
    certificate {
      store = "My"
      url = "https://vault-name45644294.vault.azure.net:443/secrets/somename/abb8ebf911a145ccacca758026efacca"
    }
    key_vault_id = data.azurerm_key_vault.KeyVault.id
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }

  network_interface {
    name    = "NetworkInterface"
    primary = true
    network_security_group_id = azurerm_network_security_group.SecurityGroup.id

    ip_configuration {
      name      = "internal"
      primary   = true
      subnet_id = azurerm_subnet.Subnet.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.BackendAddressPool.id]
      public_ip_address  {
        name = "publiciphere"
      }
    }
  }
}

data "azurerm_key_vault" "KeyVault" {
  name                = "vault-name45644294"
  resource_group_name = "resource_group"
}

data "azurerm_key_vault_secret" "somename" {
  name         = "somename"
  key_vault_id = data.azurerm_key_vault.KeyVault.id
}


